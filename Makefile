.PHONY: build

GIT_BRANCH = $$(git symbolic-ref --short HEAD)
DOCKER_UP = "export GIT_BRANCH=$(GIT_BRANCH) && docker-compose up"
DOCKER_DOWN = "export GIT_BRANCH=$(GIT_BRANCH) && docker-compose down"
DOCKER_RUN = "export GIT_BRANCH=$(GIT_BRANCH) && docker-compose run"

.EXPORT_ALL_VARIABLES:
SNOWFLAKE_SNAPSHOT_DATABASE=SNOWFLAKE
SNOWFLAKE_LOAD_DATABASE=RAW
SNOWFLAKE_PREP_DATABASE=PREP
SNOWFLAKE_PREP_SCHEMA=preparation
SNOWFLAKE_PROD_DATABASE=PROD
SNOWFLAKE_TRANSFORM_WAREHOUSE=ANALYST_XS

.DEFAULT: help
help:
	@echo "\n \
	------------------------------ \n \
 	\n \
	++ jupyter Related ++ \n \
	jupyter: Spins up a jupyter instance hosted at http://127.0.0.1:8888/\n \
	\n \
	++ Python Related ++ \n \
	lint: Runs a linter (Black) over the whole repo. \n \
	\n \
	++ Utilities ++ \n \
	cleanup: WARNING: DELETES DB VOLUME, frees up space and gets rid of old containers/images. \n \
	------------------------------ \n"

jupyter:
	@"$(DOCKER_DOWN)"
	@"$(DOCKER_UP)"

cleanup:
	@echo "Cleaning things up..."
	@"$(DOCKER_DOWN)" -v
	@docker system prune -f

lint:
	@echo "Linting the repo..."
	@pipenv run black .

prepare-pipenv:
	@echo "Setting up pipenv and installing packages"
	@which pipenv || python3 -m pip install pipenv

setup-jupyter-local: prepare-pipenv
	@echo "Setting up local Jupyter"
	@curl -L -O "https://github.com/conda-forge/miniforge/releases/download/4.12.0-2/Mambaforge-Darwin-x86_64.sh"
	@bash Mambaforge-Darwin-x86_64.sh -u -b -f
	@rm Mambaforge-Darwin-x86_64.sh
	@pipenv --python=${HOME}/mambaforge/bin/python install
	@zsh admin/setup.zsh
	@echo "Data Science environment successfully created"

jupyter-local:
	@echo "Running local Jupyter"
	@pipenv run jupyter-lab

setup-jupyter-local-no-mamba: prepare-pipenv
	@echo "Setting up local Jupyter without mamba"
	@pipenv install