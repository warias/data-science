WITH 

--Define our population
-- Unique at the namespace_id
trial_namespaces AS (
  SELECT a.gl_namespace_id as namespace_id
     , a.glm_source
     , a.glm_content
     , a.trial_entity
     , a.start_date
     , CASE WHEN c.is_setup_for_company = TRUE THEN 'Yes'
            WHEN c.is_setup_for_company = FALSE THEN 'No'
            ELSE NULL END AS is_setup_for_company_flag
     , c.visibility_level
     , c.creator_id
    FROM PROD.legacy.customers_db_trial_histories a
    LEFT JOIN (SELECT DISTINCT b.dim_namespace_id -- Exclude any trial account that has already converted or are already a paid account before the snapshot period ends
               FROM PROD.common.dim_order_hist b
               WHERE b.order_is_trial = FALSE -- Not a trial subscription
               AND amendment_type in ('NewSubscription', 'NewProduct')
               AND b.order_created_at <= '{snapshot_dt}'::DATE
               --AND b.order_created_at BETWEEN '{period_start_dt}'::DATE AND '{snapshot_dt}'::DATE  
               ) b2
      ON a.gl_namespace_id = b2.dim_namespace_id
    INNER JOIN prod.common.dim_namespace c
      ON a.gl_namespace_id = c.dim_namespace_id
      AND c.namespace_type = 'Group'
    WHERE a.start_date BETWEEN '{period_start_dt}'::DATE AND '{snapshot_dt}'::DATE -- Get list of all namespaces that have started a trial in the last 60 days before the snapshot end date
      AND c.NAMESPACE_IS_INTERNAL = FALSE -- Remove gitlab internal namespaces
      AND c.namespace_is_ultimate_parent = TRUE -- Limit to just namespace that don't have parent namespaces
      AND b2.dim_namespace_id IS NULL -- Exclude any trial account that has already converted or are already a paid account before the snapshot period ends
  ),
  
  -- Define our outcome
  -- Unique at namespace_id converted
paid_namespaces AS (
  select distinct a.namespace_id
  from trial_namespaces a
  inner join PROD.common.dim_order_hist b
    ON a.namespace_id = b.dim_namespace_id
  where b.dim_subscription_id IS NOT NULL -- Order must have valid associated subscription_id
     AND b.order_is_trial = FALSE -- Not a trial subscription
     AND amendment_type in ('NewSubscription', 'NewProduct')
     AND b.order_created_at > '{snapshot_dt}'::DATE AND b.order_created_at <= '{prediction_dt}'::DATE -- Conversion must occur after our snapshot date and before the end of the prediction window
     AND b.order_created_at >= a.start_date -- Ensure that order happens after start date
    ),

-- Pull in creator data
-- Unique at creator_id/user_id
creator_data AS (
  SELECT  a.creator_id  
    , CASE b.REGISTRATION_OBJECTIVE
        WHEN 0 THEN 'basics'
        WHEN 1 THEN 'move_repository'
        WHEN 2 THEN 'code_storage'
        WHEN 3 THEN 'exploring_to_switch'
        WHEN 4 THEN 'ci'
        WHEN 5 THEN 'other'
        WHEN 6 THEN 'joining_team'
        ELSE NULL
        END as registration_objective
    , regexp_substr(c.email, '@(.+)', 1, 1, 'e') as email_domain
    , CASE when email_domain is null then 'missing'
                when email_domain like '%yahoo%'
                or email_domain like '%rediff%'
                or email_domain like '%gmail%'
                or email_domain like '%hotmail%'
                or email_domain like '%outlook%'
                or email_domain like '%verizon.net%'
                or email_domain like '%live.%'
                or email_domain like '%sbcglobal.net%'
                or email_domain like '%laposte%'
                or email_domain like '%pm.me%'
                or email_domain like '%inbox%'
                or email_domain like '%yandex%'
                or email_domain like '%fastmail.%'
                or email_domain like '%protonmail%'
                or email_domain like '%email.%'
                or email_domain like '%att.net%'
                or email_domain like '%posteo%'
                or email_domain like '%rocketmail%'
                or email_domain like '%bk.ru%'
                or email_domain in ('gmail.com',
                                      'qq.com',
                                      'hotmail.com',
                                      '',
                                      'yahoo.com',
                                      'outlook.com',
                                      '163.com',
                                      'mail.ru',
                                      'googlemail.com',
                                      'yandex.ru',
                                      'protonmail.com',
                                      'icloud.com',
                                      't-mobile.com',
                                      'example.com',
                                      'live.com',
                                      '126.com',
                                      'me.com',
                                      'gmx.de',
                                      'hotmail.fr',
                                      'web.de',
                                      'google.com',
                                      'yahoo.fr',
                                      'naver.com',
                                      'foxmail.com',
                                      'aol.com',
                                      'msn.com',
                                      'hotmail.co.uk',
                                      'ya.ru',
                                      'wp.pl',
                                      'gmx.net',
                                      'live.fr',
                                      'ymail.com',
                                      'orange.fr',
                                      'yahoo.co.uk',
                                      'ancestry.com',
                                      'free.fr',
                                      'comcast.net',
                                      'hotmail.de',
                                      'mail.com',
                                      'ukr.net',
                                      'yahoo.co.jp',
                                      'mac.com',
                                      'yahoo.co.in',
                                      'gitlab.com',
                                      'yahoo.com.br',
                                      'gitlab.localhost') then 'Personal' else 'Business' end as creator_email_type
    , b.job_title as creator_job_title
  FROM (SELECT DISTINCT creator_id FROM trial_namespaces) a 
  LEFT JOIN PROD.legacy.gitlab_dotcom_user_details b 
    ON a.creator_id = b.user_id
  LEFT JOIN PROD.legacy.gitlab_dotcom_users_xf c
    ON a.creator_id = c.user_id

  ),

--Pull in User Data
user_data AS (

   SELECT a.namespace_id
    , COUNT(1)                                                                        AS current_member_cnt
    , COUNT(IFF(role = 'Software Developer', role, NULL))                             AS software_developer_member_cnt
    , COUNT(IFF(role = 'Systems Administrator', role, NULL))                          AS system_administrator_member_cnt
    , COUNT(IFF(role = 'Devops Engineer', role, NULL))                                AS devops_engineer_member_cnt
    , COUNT(IFF(role = 'Development Team Lead', role, NULL))                          AS development_team_lead_member_cnt
    , COUNT(IFF(role = 'Product Manager', role, NULL))                                AS product_manager_member_cnt
    , COUNT(IFF(role = 'Security Analyst', role, NULL))                               AS security_analyst_member_cnt
    , COUNT(IFF(role = 'Data Analyst', role, NULL))                                   AS data_analyst_member_cnt
    , COUNT(IFF(role = 'Other', role, NULL))                                          AS other_member_cnt
    , COUNT(IFF(it_job_title_hierarchy = 'IT Individual Contributor', role, NULL))    AS it_ic_member_cnt
    , SUM(SIGN_IN_COUNT) as sign_in_cnt
    
    , MAX(CASE WHEN IS_EMAIL_OPTED_IN = 'TRUE' AND EMAIL_OPTED_IN_AT <= '{snapshot_dt}'::DATE THEN 1 ELSE 0 END) email_opt_in_flag
    , SUM(CASE WHEN preferred_language = 'en' THEN 1 ELSE 0 END) AS pref_lang_en_cnt
    , CAST(sign_in_cnt AS FLOAT) / CAST(current_member_cnt AS FLOAT) AS avg_sign_ins_per_member
   FROM trial_namespaces a
   INNER JOIN (SELECT DISTINCT ultimate_parent_id, user_id FROM prod.legacy.gitlab_dotcom_memberships) b -- Only keep user_ids that are in our trial population
      ON a.namespace_id = b.ultimate_parent_id
   INNER JOIN "PREP"."GITLAB_DOTCOM"."GITLAB_DOTCOM_USERS_SOURCE" c
      ON b.user_id = c.user_id
      AND CREATED_AT <= '{snapshot_dt}'::DATE
   GROUP BY a.namespace_id 
),

--Pull in snowplow activity
snowplow_activity AS (

  WITH

  raw_snowplow_activity AS (
   --Pull in current month of snowplow website activity
    SELECT b.namespace_id
       , a.derived_tstamp
       , IFNULL(a.GSC_GOOGLE_ANALYTICS_CLIENT_ID,
                  CASE WHEN lead(a.GSC_GOOGLE_ANALYTICS_CLIENT_ID, 4, '0') over (partition by a.GSC_PSEUDONYMIZED_USER_ID order by a.derived_tstamp) = 0 THEN NULL 
                  ELSE lead(a.GSC_GOOGLE_ANALYTICS_CLIENT_ID, 4, '0') over (partition by a.GSC_PSEUDONYMIZED_USER_ID order by a.derived_tstamp) end) AS GSC_GOOGLE_ANALYTICS_CLIENT_ID
       , a.GSC_PSEUDONYMIZED_USER_ID --ga info
       , a.gsc_plan
       , a.gsc_source
       , a.gsc_project_id
       , a.device_type
       , a.os_timezone
       , a.browser_language
       , a.os_manufacturer
       , a.page_url
       , a.event_action
       , a.event_category
       , a.event_label
       , a.event_property
       , a.event_value
       , CASE WHEN a.event_action = 'render' and a.EVENT_CATEGORY = 'subscriptions:new' and a.event_label = 'saas_checkout' THEN 'start SaaS Checkout'
            ELSE NULL END as start_saas_checkout_flag
    FROM prep.snowplow_2022_01.snowplow_structured_events a
    INNER JOIN trial_namespaces b
       ON a.gsc_namespace_id = b.namespace_id
    LEFT JOIN (select distinct event_id 
               from prep.SNOWPLOW_2022_01.SNOWPLOW_GITLAB_EVENTS_EXPERIMENT_CONTEXTS)c
       ON a.event_id = c.event_id
    WHERE gsc_pseudonymized_user_id != '580098ad03a961096aed83cf4f12b35f72626701d793dab1c33a07d7493878f4' --Null users were initially hard-coded to this user_id. Fixed in future months
       AND derived_tstamp <= '{snapshot_dt}'::DATE 
       AND GSC_PLAN in ('free', 'ultimate_trial', 'premium_trial')
       AND app_id = 'gitlab' -- Remove staging app
       AND GSC_ENVIRONMENT = 'production'
       AND EVENT_CATEGORY != 'billing_in_side_nav' -- remove experiment event
       AND EVENT_CATEGORY != 'subscription_card' -- only fires when a subscription auto renews or cancels, useless
       AND NOT (event_label = 'main_navigation' and event_property = 'navigation' and event_action like '%click%') -- can happen from anywhere where you click the logo to navigate back to home, not insightful
       AND (event_label != 'projects_side_navigation' and EVENT_ACTION != 'render' and event_property != 'projects_side_navigation') -- render side menu is not insightful regardless of category
       AND c.event_id IS NULL -- ensure just production-level events 
    
    --Pull in previous month of snowplow website_activity
  
    UNION ALL
    
    SELECT b.namespace_id
       , a.derived_tstamp
       , IFNULL(a.GSC_GOOGLE_ANALYTICS_CLIENT_ID,
                  CASE WHEN lead(a.GSC_GOOGLE_ANALYTICS_CLIENT_ID, 4, '0') over (partition by a.GSC_PSEUDONYMIZED_USER_ID order by a.derived_tstamp) = 0 THEN NULL 
                  ELSE lead(a.GSC_GOOGLE_ANALYTICS_CLIENT_ID, 4, '0') over (partition by a.GSC_PSEUDONYMIZED_USER_ID order by a.derived_tstamp) end) AS GSC_GOOGLE_ANALYTICS_CLIENT_ID
       , a.GSC_PSEUDONYMIZED_USER_ID --ga info
       , a.gsc_plan
       , a.gsc_source
       , a.gsc_project_id
       , a.device_type
       , a.os_timezone
       , a.browser_language
       , a.os_manufacturer
       , a.page_url
       , a.event_action
       , a.event_category
       , a.event_label
       , a.event_property
       , a.event_value
       , CASE WHEN a.event_action = 'render' and a.EVENT_CATEGORY = 'subscriptions:new' and a.event_label = 'saas_checkout' THEN 'start_saas_checkout'
            ELSE NULL END as start_saas_checkout_flag
    FROM prep.snowplow_2021_12.snowplow_structured_events a
    INNER JOIN trial_namespaces b
       ON a.gsc_namespace_id = b.namespace_id
    LEFT JOIN (select distinct event_id 
               from prep.SNOWPLOW_2021_12.SNOWPLOW_GITLAB_EVENTS_EXPERIMENT_CONTEXTS)c
       ON a.event_id = c.event_id
    WHERE gsc_pseudonymized_user_id != '580098ad03a961096aed83cf4f12b35f72626701d793dab1c33a07d7493878f4' --Null users were initially hard-coded to this user_id. Fixed in future months
       --AND derived_tstamp <= '{snapshot_dt}'::DATE -- Not needed because previous month will only have dates prior to snapshot_dt
       AND GSC_PLAN in ('free', 'ultimate_trial', 'premium_trial')
       AND app_id = 'gitlab' -- Remove staging app
       AND GSC_ENVIRONMENT = 'production'
       AND EVENT_CATEGORY != 'billing_in_side_nav' -- remove experiment event
       AND EVENT_CATEGORY != 'subscription_card' -- only fires when a subscription auto renews or cancels, useless
       AND NOT (event_label = 'main_navigation' and event_property = 'navigation' and event_action like '%click%') -- can happen from anywhere where you click the logo to navigate back to home, not insightful
       AND (event_label != 'projects_side_navigation' and EVENT_ACTION != 'render' and event_property != 'projects_side_navigation') -- render side menu is not insightful regardless of category
       AND c.event_id IS NULL -- ensure just production-level events 

    )
  
  --Aggregate data by namespace
  SELECT namespace_id
      , COUNT(distinct event_category) as distinct_event_categories_cnt_recent
      , COUNT(distinct gsc_project_id) as projects_cnt_recent
      , COUNT(distinct gsc_pseudonymized_user_id) as users_cnt_recent
      , SUM(CASE WHEN start_saas_checkout_flag is not null then 1 else 0 end) as start_saas_checkout_cnt_recent
      , SUM(CASE WHEN event_category ilike '%Pypi%' or page_url ilike '%.py%' or EVENT_ACTION ilike '%pypi%'
          or EVENT_ACTION ilike '%pip%' or EVENT_CATEGORY ilike '%pip%'
          or EVENT_LABEL ilike '%pip%' or EVENT_PROPERTY ilike '%pip%' then 1 else 0 end) as python_activity_cnt_recent
      , SUM(CASE WHEN regexp_like(page_url, 'data[*.]science', 'i') or regexp_like(page_url, 'machine[*.]learn', 'i') or regexp_like(page_url, 'deep[*.]learn', 'i')
          or regexp_like(page_url, 'neural[*.]net', 'i') or regexp_like(page_url, 'reinforcement[*.]learn', 'i')
          then 1 else 0 end) as datasci_activity_cnt_recent
      , SUM(CASE WHEN event_category ilike '%npm%' or page_url like '%.css%' or page_url like '%.js%' or EVENT_ACTION like '%npm%'
          or EVENT_LABEL like '%npm%' or EVENT_PROPERTY like '%npm%' then 1 else 0 end) as javascript_activity_cnt_recent
      , SUM(CASE WHEN event_category ilike '%maven%' or page_url like '%.java%' or page_url like '%.jar%' or EVENT_ACTION like '%maven%'
          or EVENT_LABEL like '%maven%' or EVENT_PROPERTY like '%maven%' then 1 else 0 end) as java_activity_cnt_recent
      , SUM(CASE WHEN EVENT_CATEGORY ilike '%API::ProjectContainerRepositories%' or EVENT_CATEGORY ilike '%container_registry%' then 1 else 0 end)
          as docker_registry_api_activity_cnt_recent
      , SUM(CASE WHEN EVENT_CATEGORY ilike '%gcp%' then 1 else 0 end) as gcp_activity_cnt_recent
      , SUM(CASE WHEN EVENT_CATEGORY ilike '%snippet%' or EVENT_ACTION like '%snippet%'
          or EVENT_LABEL like '%snippet%' then 1 else 0 end) as snippet_activity_cnt_recent
      , SUM(CASE WHEN EVENT_PROPERTY ilike '%kubernetes%' or EVENT_LABEL ilike '%kubernetes%' then 1 else 0 end) as kubernetes_activity_cnt_recent
      , SUM(CASE WHEN EVENT_PROPERTY ilike '%logs%' or EVENT_LABEL ilike '%logs%'
          or event_category ilike '%logs%' then 1 else 0 end) as logs_activity_cnt_recent
      , SUM(CASE WHEN event_category ilike '%logs%' and event_label ilike '%pod%' then 1 else 0 end) as logs_pods_activity_cnt_recent
      , SUM(CASE WHEN EVENT_PROPERTY ilike '%cluster%' or EVENT_LABEL ilike '%cluster%'
          or event_action ilike '%cluster%' or event_category ilike '%cluster' then 1 else 0 end) as cluster_activity_cnt_recent
      , SUM(CASE WHEN EVENT_PROPERTY ilike '%agent_registration%' or EVENT_LABEL ilike '%agent_registration%'
          or event_action ilike '%agent_registration%' then 1 else 0 end) as agent_registration_activity_cnt_recent
      , SUM(CASE WHEN os_timezone like 'Asia%' THEN 1 ELSE 0 END) AS asia_event_cnt_recent
      , SUM(CASE WHEN os_timezone like 'America%' THEN 1 ELSE 0 END) AS america_event_cnt_recent
      , SUM(CASE WHEN os_timezone like 'Africa%' THEN 1 ELSE 0 END) AS africa_event_cnt_recent
      , SUM(CASE WHEN os_timezone like 'Europe%' THEN 1 ELSE 0 END) AS europe_event_cnt_recent
      , SUM(CASE WHEN os_timezone like 'UTC' THEN 1 ELSE 0 END) AS utc_event_cnt_recent
      , SUM(CASE WHEN OS_MANUFACTURER = 'Microsoft Corporation' THEN 1 ELSE 0 END) AS os_microsoft_event_cnt_recent
      , SUM(CASE WHEN OS_MANUFACTURER = 'Apple Inc.' THEN 1 ELSE 0 END) AS os_apple_event_cnt_recent
      , SUM(CASE WHEN OS_MANUFACTURER = 'Canonical Ltd.' THEN 1 ELSE 0 END) AS os_linux_event_cnt_recent
      , SUM(CASE WHEN OS_MANUFACTURER = 'Google Inc.' THEN 1 ELSE 0 END) AS os_chrome_event_cnt_recent
      , SUM(CASE WHEN OS_MANUFACTURER = 'Other' THEN 1 ELSE 0 END) AS os_other_event_cnt_recent
  FROM raw_snowplow_activity
  GROUP BY namespace_id
    
  ),

-- Pull in Lastest moth of product usage data
product_usage_curr_month AS (
  SELECT a.*
  FROM PROD.common_mart_product.mart_product_usage_free_user_metrics_monthly a
  INNER JOIN trial_namespaces b
    ON a.dim_namespace_id = b.namespace_id
  WHERE a.reporting_month = DATE_TRUNC('MONTH', '{snapshot_dt}'::DATE)

  ),

product_usage_prev_month AS (
  SELECT a.*
  FROM PROD.common_mart_product.mart_product_usage_free_user_metrics_monthly a
  INNER JOIN trial_namespaces b
    ON a.dim_namespace_id = b.namespace_id
  WHERE a.reporting_month = DATE_TRUNC('MONTH', DATEADD('MONTH', -1, '{snapshot_dt}'::DATE))

  ),

stage_usage_curr_month AS (
  SELECT a.*
  FROM PROD.workspace_data_science.monthly_stage_usage_by_namespace a
  INNER JOIN trial_namespaces b
    ON a.dim_namespace_id = b.namespace_id
  WHERE snapshot_month = DATE_TRUNC('MONTH', '{snapshot_dt}'::DATE)

  ),

stage_usage_prev_month AS (
  SELECT a.*
  FROM PROD.workspace_data_science.monthly_stage_usage_by_namespace a
  INNER JOIN trial_namespaces b
    ON a.dim_namespace_id = b.namespace_id
  WHERE snapshot_month = DATE_TRUNC('MONTH', DATEADD('MONTH', -1, '{snapshot_dt}'::DATE))

  ),

-- Pull in Usage Order
usage_order AS (

  WITH 
  
    usage_ts as (
        SELECT a.NAMESPACE_ID
           , a.STAGE_NAME
           , TO_VARCHAR(min(a.EVENT_CREATED_AT), 'YYYY-MM-DD HH24') as first_event_tstamp
           , dense_rank() over (partition by a.NAMESPACE_ID order by first_event_tstamp) as rank_order_by_time_event
        FROM prod.legacy.GITLAB_DOTCOM_USAGE_DATA_EVENTS a
        INNER JOIN trial_namespaces b
           ON a.namespace_id = b.namespace_id
        WHERE DATE_TRUNC('DAY', a.event_created_at) <= '{snapshot_dt}'::DATE
        GROUP BY a.NAMESPACE_ID, a.STAGE_NAME
        ORDER BY a.NAMESPACE_ID, first_event_tstamp
      
      )

  select namespace_id
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 1 AND STAGE_NAME = 'create' THEN 1 ELSE 0 END) AS first_stage_create
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 1 AND STAGE_NAME = 'protect' THEN 1 ELSE 0 END) AS first_stage_protect
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 1 AND STAGE_NAME = 'package' THEN 1 ELSE 0 END) AS first_stage_package
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 1 AND STAGE_NAME = 'monitor' THEN 1 ELSE 0 END) AS first_stage_monitor
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 1 AND STAGE_NAME = 'manage' THEN 1 ELSE 0 END) AS first_stage_manage
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 1 AND STAGE_NAME = 'plan' THEN 1 ELSE 0 END) AS first_stage_plan
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 1 AND STAGE_NAME = 'secure' THEN 1 ELSE 0 END) AS first_stage_secure
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 1 AND STAGE_NAME = 'verify' THEN 1 ELSE 0 END) AS first_stage_verify
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 1 AND STAGE_NAME = 'configure' THEN 1 ELSE 0 END) AS first_stage_configure
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 1 AND STAGE_NAME = 'release' THEN 1 ELSE 0 END) AS first_stage_release
      
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 2 AND STAGE_NAME = 'create' THEN 1 ELSE 0 END) AS second_stage_create
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 2 AND STAGE_NAME = 'protect' THEN 1 ELSE 0 END) AS second_stage_protect
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 2 AND STAGE_NAME = 'package' THEN 1 ELSE 0 END) AS second_stage_package
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 2 AND STAGE_NAME = 'monitor' THEN 1 ELSE 0 END) AS second_stage_monitor
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 2 AND STAGE_NAME = 'manage' THEN 1 ELSE 0 END) AS second_stage_manage
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 2 AND STAGE_NAME = 'plan' THEN 1 ELSE 0 END) AS second_stage_plan
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 2 AND STAGE_NAME = 'secure' THEN 1 ELSE 0 END) AS second_stage_secure
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 2 AND STAGE_NAME = 'verify' THEN 1 ELSE 0 END) AS second_stage_verify
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 2 AND STAGE_NAME = 'configure' THEN 1 ELSE 0 END) AS second_stage_configure
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 2 AND STAGE_NAME = 'release' THEN 1 ELSE 0 END) AS second_stage_release
      
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 3 AND STAGE_NAME = 'create' THEN 1 ELSE 0 END) AS third_stage_create
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 3 AND STAGE_NAME = 'protect' THEN 1 ELSE 0 END) AS third_stage_protect
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 3 AND STAGE_NAME = 'package' THEN 1 ELSE 0 END) AS third_stage_package
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 3 AND STAGE_NAME = 'monitor' THEN 1 ELSE 0 END) AS third_stage_monitor
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 3 AND STAGE_NAME = 'manage' THEN 1 ELSE 0 END) AS third_stage_manage
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 3 AND STAGE_NAME = 'plan' THEN 1 ELSE 0 END) AS third_stage_plan
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 3 AND STAGE_NAME = 'secure' THEN 1 ELSE 0 END) AS third_stage_secure
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 3 AND STAGE_NAME = 'verify' THEN 1 ELSE 0 END) AS third_stage_verify
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 3 AND STAGE_NAME = 'configure' THEN 1 ELSE 0 END) AS third_stage_configure
      , MAX(CASE WHEN RANK_ORDER_BY_TIME_EVENT = 3 AND STAGE_NAME = 'release' THEN 1 ELSE 0 END) AS third_stage_release
  from usage_ts
  group by namespace_id

  )

-- Final modeling dataset. Returns all namespaces that had a trial account within 30 days prior to snapshot_dt and also have usage data
SELECT a.namespace_id
  , a.glm_source
  , a.glm_content
  , a.trial_entity
  , COALESCE(a.is_setup_for_company_flag, 'missing') AS is_setup_for_company_flag
  , COALESCE(a.visibility_level, 'Unknown') AS visibility_level
  , DATEDIFF('MONTH', a.start_date, '{snapshot_dt}'::DATE) AS namespace_tenure_months
  , DATEDIFF('DAY', a.start_date, '{snapshot_dt}'::DATE) AS namespace_tenure_days

  , CASE WHEN b.namespace_id IS NOT NULL THEN 1 ELSE 0 END AS dv_purchase_flag

  --User Data
  , c.REGISTRATION_OBJECTIVE
  , c.creator_email_type
  , c.creator_job_title
  , c.email_domain AS creator_email_domain
  , COALESCE(c2.current_member_cnt,1) as current_member_cnt
  , COALESCE(c2.software_developer_member_cnt,0) as software_developer_member_cnt
  , COALESCE(c2.system_administrator_member_cnt,0) as system_administrator_member_cnt
  , COALESCE(c2.devops_engineer_member_cnt,0) as devops_engineer_member_cnt
  , COALESCE(c2.development_team_lead_member_cnt,0) as development_team_lead_member_cnt
  , COALESCE(c2.product_manager_member_cnt,0) as product_manager_member_cnt
  , COALESCE(c2.security_analyst_member_cnt,0) as security_analyst_member_cnt
  , COALESCE(c2.data_analyst_member_cnt,0) as data_analyst_member_cnt
  , COALESCE(c2.other_member_cnt,0) as other_member_cnt
  , COALESCE(c2.it_ic_member_cnt,0) as it_ic_member_cnt
  , COALESCE(c2.sign_in_cnt,0) as sign_in_cnt
  , COALESCE(c2.email_opt_in_flag,0) as email_opt_in_flag
  , COALESCE(c2.pref_lang_en_cnt,0) as pref_lang_en_cnt
  , COALESCE(c2.avg_sign_ins_per_member,0) as avg_sign_ins_per_member


  --Snowplow data
  , COALESCE(d.distinct_event_categories_cnt_recent,0) as distinct_event_categories_cnt_recent
  , COALESCE(d.projects_cnt_recent,0) as projects_cnt_recent
  , COALESCE(d.users_cnt_recent,0) as users_cnt_recent
  , COALESCE(d.start_saas_checkout_cnt_recent,0) as start_saas_checkout_cnt_recent
  , COALESCE(d.datasci_activity_cnt_recent,0) as datasci_activity_cnt_recent
  , COALESCE(d.javascript_activity_cnt_recent,0) as javascript_activity_cnt_recent
  , COALESCE(d.java_activity_cnt_recent,0) as java_activity_cnt_recent
  , COALESCE(d.docker_registry_api_activity_cnt_recent,0) as docker_registry_api_activity_cnt_recent
  , COALESCE(d.gcp_activity_cnt_recent,0) as gcp_activity_cnt_recent
  , COALESCE(d.snippet_activity_cnt_recent,0) as snippet_activity_cnt_recent
  , COALESCE(d.kubernetes_activity_cnt_recent,0) as kubernetes_activity_cnt_recent
  , COALESCE(d.logs_activity_cnt_recent,0) as logs_activity_cnt_recent
  , COALESCE(d.logs_pods_activity_cnt_recent,0) as logs_pods_activity_cnt_recent
  , COALESCE(d.cluster_activity_cnt_recent,0) as cluster_activity_cnt_recent
  , COALESCE(d.agent_registration_activity_cnt_recent,0) as agent_registration_activity_cnt_recent
  , COALESCE(d.asia_event_cnt_recent,0) as asia_event_cnt_recent
  , COALESCE(d.america_event_cnt_recent,0) as america_event_cnt_recent
  , COALESCE(d.africa_event_cnt_recent,0) as africa_event_cnt_recent
  , COALESCE(d.europe_event_cnt_recent,0) as europe_event_cnt_recent
  , COALESCE(d.utc_event_cnt_recent,0) as utc_event_cnt_recent
  , COALESCE(d.os_microsoft_event_cnt_recent,0) as os_microsoft_event_cnt_recent
  , COALESCE(d.os_apple_event_cnt_recent,0) as os_apple_event_cnt_recent
  , COALESCE(d.os_linux_event_cnt_recent,0) as os_linux_event_cnt_recent
  , COALESCE(d.os_chrome_event_cnt_recent,0) as os_chrome_event_cnt_recent
  , COALESCE(d.os_other_event_cnt_recent,0) as os_other_event_cnt_recent

  -- Pull in last active state of product features
  , CASE WHEN e.gitlab_shared_runners_enabled = TRUE OR f.gitlab_shared_runners_enabled = TRUE THEN 1 ELSE 0 END AS gitlab_shared_runners_enabled
  , CASE WHEN e.container_registry_enabled = TRUE OR f.container_registry_enabled = TRUE THEN 1 ELSE 0 END AS container_registry_enabled
  , CASE WHEN e.auto_devops_enabled = TRUE OR f.auto_devops_enabled = 'TRUE' THEN 1 ELSE 0 END AS auto_devops_enabled

  --Pull in all-time usage data. If no data from the current month then use the previous month
  , COALESCE(e.commit_comment_all_time_event, f.commit_comment_all_time_event, 0) AS commit_comment_all_time_event
  , COALESCE(e.source_code_pushes_all_time_event, f.source_code_pushes_all_time_event, 0) AS source_code_pushes_all_time_event
  , COALESCE(e.ci_builds_all_time_user, f.ci_builds_all_time_user, 0) AS ci_builds_all_time_user
  , COALESCE(e.ci_builds_all_time_event, f.ci_builds_all_time_event, 0) AS ci_builds_all_time_event
  , COALESCE(e.ci_runners_all_time_event, f.ci_runners_all_time_event, 0) AS ci_runners_all_time_event
  , COALESCE(e.projects_with_repositories_enabled_all_time_event, f.projects_with_repositories_enabled_all_time_event, 0) AS projects_with_repositories_enabled_all_time_event
  , COALESCE(e.auto_devops_enabled_all_time_event, f.auto_devops_enabled_all_time_event, 0) AS auto_devops_enabled_all_time_event
  , COALESCE(e.template_repositories_all_time_event, f.template_repositories_all_time_event, 0) AS template_repositories_all_time_event
  , COALESCE(e.projects_with_packages_all_time_event, f.projects_with_packages_all_time_event, 0) AS projects_with_packages_all_time_event
  , COALESCE(e.merge_requests_all_time_event, f.merge_requests_all_time_event, 0) AS merge_requests_all_time_event
  , COALESCE(e.epics_all_time_event, f.epics_all_time_event, 0) AS epics_all_time_event
  , COALESCE(e.issues_all_time_event, f.issues_all_time_event, 0) AS issues_all_time_event
  , COALESCE(e.projects_all_time_event, f.projects_all_time_event, 0) AS projects_all_time_event
  , COALESCE(e.protected_branches_all_time_event, f.protected_branches_all_time_event, 0) AS protected_branches_all_time_event
  , COALESCE(e.ci_pipelines_all_time_user, f.ci_pipelines_all_time_user, 0) AS ci_pipelines_all_time_user
  , COALESCE(e.projects_with_repositories_enabled_all_time_user, f.projects_with_repositories_enabled_all_time_user, 0) AS projects_with_repositories_enabled_all_time_user
  , COALESCE(e.dependency_scanning_jobs_all_time_user, f.dependency_scanning_jobs_all_time_user, 0) AS dependency_scanning_jobs_all_time_user
  , COALESCE(e.auto_devops_pipelines_all_time_event, f.auto_devops_pipelines_all_time_event, 0) AS auto_devops_pipelines_all_time_event

  --Pull in all-time stage data. If no data from the current month then use the previous month
  , COALESCE(g.stage_plan_alltime_features, h.stage_plan_alltime_features, 0) AS stage_plan_alltime_features
  , COALESCE(g.stage_create_alltime_features, h.stage_create_alltime_features, 0) AS stage_create_alltime_features
  , COALESCE(g.stage_verify_alltime_features, h.stage_verify_alltime_features, 0) AS stage_verify_alltime_features
  , COALESCE(g.stage_package_alltime_features, h.stage_package_alltime_features, 0) AS stage_package_alltime_features
  , COALESCE(g.stage_release_alltime_features, h.stage_release_alltime_features, 0) AS stage_release_alltime_features
  , COALESCE(g.stage_configure_alltime_features, h.stage_configure_alltime_features, 0) AS stage_configure_alltime_features
  , COALESCE(g.stage_monitor_alltime_features, h.stage_monitor_alltime_features, 0) AS stage_monitor_alltime_features
  , COALESCE(g.stage_manage_alltime_features, h.stage_manage_alltime_features, 0) AS stage_manage_alltime_features
  , COALESCE(g.stage_secure_alltime_features, h.stage_secure_alltime_features, 0) AS stage_secure_alltime_features
  , COALESCE(g.stage_protect_alltime_features, h.stage_protect_alltime_features, 0) AS stage_protect_alltime_features
  , COALESCE(g.stage_ecosystem_alltime_features, h.stage_ecosystem_alltime_features, 0) AS stage_ecosystem_alltime_features
  , COALESCE(g.stage_growth_alltime_features, h.stage_growth_alltime_features, 0) AS stage_growth_alltime_features
  , COALESCE(g.stage_enablement_alltime_features, h.stage_enablement_alltime_features, 0) AS stage_enablement_alltime_features
  , COALESCE(g.section_dev_alltime_features, h.section_dev_alltime_features, 0) AS section_dev_alltime_features
  , COALESCE(g.section_enablement_alltime_features, h.section_enablement_alltime_features, 0) AS section_enablement_alltime_features
  , COALESCE(g.section_fulfillment_alltime_features, h.section_fulfillment_alltime_features, 0) AS section_fulfillment_alltime_features
  , COALESCE(g.section_growth_alltime_features, h.section_growth_alltime_features, 0) AS section_growth_alltime_features
  , COALESCE(g.section_ops_alltime_features, h.section_ops_alltime_features, 0) AS section_ops_alltime_features
  , COALESCE(g.section_sec_alltime_features, h.section_sec_alltime_features, 0) AS section_sec_alltime_features
  , COALESCE(g.section_seg_alltime_features, h.section_seg_alltime_features, 0) AS section_seg_alltime_features
  , COALESCE(g.tier_free_alltime_features, h.tier_free_alltime_features, 0) AS tier_free_alltime_features
  , COALESCE(g.tier_premium_alltime_features, h.tier_premium_alltime_features, 0) AS tier_premium_alltime_features
  , COALESCE(g.tier_ultimate_alltime_features, h.tier_ultimate_alltime_features, 0) AS tier_ultimate_alltime_features
  , COALESCE(g.stage_plan_alltime_feature_sum, h.stage_plan_alltime_feature_sum, 0) AS stage_plan_alltime_feature_sum
  , COALESCE(g.stage_create_alltime_feature_sum, h.stage_create_alltime_feature_sum, 0) AS stage_create_alltime_feature_sum
  , COALESCE(g.stage_verify_alltime_feature_sum, h.stage_verify_alltime_feature_sum, 0) AS stage_verify_alltime_feature_sum
  , COALESCE(g.stage_package_alltime_feature_sum, h.stage_package_alltime_feature_sum, 0) AS stage_package_alltime_feature_sum
  , COALESCE(g.stage_release_alltime_feature_sum, h.stage_release_alltime_feature_sum, 0) AS stage_release_alltime_feature_sum
  , COALESCE(g.stage_configure_alltime_features_sum, h.stage_configure_alltime_features_sum, 0) AS stage_configure_alltime_features_sum
  , COALESCE(g.stage_monitor_alltime_features_sum, h.stage_monitor_alltime_features_sum, 0) AS stage_monitor_alltime_features_sum
  , COALESCE(g.stage_manage_alltime_feature_sum, h.stage_manage_alltime_feature_sum, 0) AS stage_manage_alltime_feature_sum
  , COALESCE(g.stage_secure_alltime_feature_sum, h.stage_secure_alltime_feature_sum, 0) AS stage_secure_alltime_feature_sum
  , COALESCE(g.stage_protect_alltime_feature_sum, h.stage_protect_alltime_feature_sum, 0) AS stage_protect_alltime_feature_sum
  , COALESCE(g.stage_ecosystem_alltime_feature_sum, h.stage_ecosystem_alltime_feature_sum, 0) AS stage_ecosystem_alltime_feature_sum
  , COALESCE(g.stage_growth_alltime_feature_sum, h.stage_growth_alltime_feature_sum, 0) AS stage_growth_alltime_feature_sum
  , COALESCE(g.stage_enablement_alltime_feature_sum, h.stage_enablement_alltime_feature_sum, 0) AS stage_enablement_alltime_feature_sum
  , COALESCE(g.all_stages_alltime_feature_sum, h.all_stages_alltime_feature_sum, 0) AS all_stages_alltime_feature_sum
  , COALESCE(g.stage_plan_alltime_share_pct, h.stage_plan_alltime_share_pct, 0) AS stage_plan_alltime_share_pct
  , COALESCE(g.stage_create_alltime_share_pct, h.stage_create_alltime_share_pct, 0) AS stage_create_alltime_share_pct
  , COALESCE(g.stage_verify_alltime_share_pct, h.stage_verify_alltime_share_pct, 0) AS stage_verify_alltime_share_pct
  , COALESCE(g.stage_package_alltime_share_pct, h.stage_package_alltime_share_pct, 0) AS stage_package_alltime_share_pct
  , COALESCE(g.stage_release_alltime_share_pct, h.stage_release_alltime_share_pct, 0) AS stage_release_alltime_share_pct
  , COALESCE(g.stage_configure_alltime_share_pct, h.stage_configure_alltime_share_pct, 0) AS stage_configure_alltime_share_pct
  , COALESCE(g.stage_monitor_alltime_share_pct, h.stage_monitor_alltime_share_pct, 0) AS stage_monitor_alltime_share_pct
  , COALESCE(g.stage_manage_alltime_share_pct, h.stage_manage_alltime_share_pct, 0) AS stage_manage_alltime_share_pct
  , COALESCE(g.stage_secure_alltime_share_pct, h.stage_secure_alltime_share_pct, 0) AS stage_secure_alltime_share_pct
  , COALESCE(g.stage_protect_alltime_share_pct, h.stage_protect_alltime_share_pct, 0) AS stage_protect_alltime_share_pct
  , COALESCE(g.stage_ecosystem_alltime_share_pct, h.stage_ecosystem_alltime_share_pct, 0) AS stage_ecosystem_alltime_share_pct
  , COALESCE(g.stage_growth_alltime_share_pct, h.stage_growth_alltime_share_pct, 0) AS stage_growth_alltime_share_pct
  , COALESCE(g.stage_enablement_alltime_share_pct, h.stage_enablement_alltime_share_pct, 0) AS stage_enablement_alltime_share_pct
  , COALESCE(g.stage_most_used_alltime, h.stage_most_used_alltime, NULL) AS stage_most_used_alltime

  
  --Pull in usage data so far this month
  , CASE WHEN COALESCE(e.commit_comment_all_time_event, 0) - COALESCE(f.commit_comment_all_time_event,0) < 0 THEN 0
      ELSE COALESCE(e.commit_comment_all_time_event, 0) - COALESCE(f.commit_comment_all_time_event,0) END AS commit_comment_event_this_month
  , CASE WHEN COALESCE(e.source_code_pushes_all_time_event, 0) - COALESCE(f.source_code_pushes_all_time_event,0) < 0 THEN 0
      ELSE COALESCE(e.source_code_pushes_all_time_event, 0) - COALESCE(f.source_code_pushes_all_time_event,0) END AS source_code_pushes_event_this_month
  , CASE WHEN COALESCE(e.ci_builds_all_time_user, 0) - COALESCE(f.ci_builds_all_time_user,0) < 0 THEN 0
      ELSE COALESCE(e.ci_builds_all_time_user, 0) - COALESCE(f.ci_builds_all_time_user,0) END AS ci_builds_all_time_user_this_month
  , CASE WHEN COALESCE(e.ci_builds_all_time_event, 0) - COALESCE(f.ci_builds_all_time_event,0) < 0 THEN 0
      ELSE COALESCE(e.ci_builds_all_time_event, 0) - COALESCE(f.ci_builds_all_time_event,0)  END AS ci_builds_event_this_month
  , CASE WHEN COALESCE(e.ci_runners_all_time_event, 0) - COALESCE(f.ci_runners_all_time_event,0) < 0 THEN 0
      ELSE COALESCE(e.ci_runners_all_time_event, 0) - COALESCE(f.ci_runners_all_time_event,0)  END AS ci_runners_event_this_month
  , CASE WHEN COALESCE(e.auto_devops_enabled_all_time_event, 0) - COALESCE(f.auto_devops_enabled_all_time_event,0) < 0 THEN 0
      ELSE COALESCE(e.auto_devops_enabled_all_time_event, 0) - COALESCE(f.auto_devops_enabled_all_time_event,0)  END AS auto_devops_enabled_event_this_month
  , CASE WHEN COALESCE(e.template_repositories_all_time_event, 0) - COALESCE(f.template_repositories_all_time_event,0) < 0 THEN 0
      ELSE COALESCE(e.template_repositories_all_time_event, 0) - COALESCE(f.template_repositories_all_time_event,0) END AS template_repositories_event_this_month
  , CASE WHEN COALESCE(e.projects_with_packages_all_time_event, 0) - COALESCE(f.projects_with_packages_all_time_event,0) < 0 THEN 0
      ELSE COALESCE(e.projects_with_packages_all_time_event, 0) - COALESCE(f.projects_with_packages_all_time_event,0) END AS projects_with_packages_event_this_month
  , CASE WHEN COALESCE(e.merge_requests_all_time_event, 0) - COALESCE(f.merge_requests_all_time_event,0) < 0 THEN 0
      ELSE COALESCE(e.merge_requests_all_time_event, 0) - COALESCE(f.merge_requests_all_time_event,0)  END AS merge_requests_event_this_month
  , CASE WHEN COALESCE(e.epics_all_time_event, 0) - COALESCE(f.epics_all_time_event,0) < 0 THEN 0
      ELSE COALESCE(e.epics_all_time_event, 0) - COALESCE(f.epics_all_time_event,0)  END AS epics_event_this_month
  , CASE WHEN COALESCE(e.issues_all_time_event, 0) - COALESCE(f.issues_all_time_event,0) < 0 THEN 0
      ELSE COALESCE(e.issues_all_time_event, 0) - COALESCE(f.issues_all_time_event,0)  END AS issues_event_this_month
  , CASE WHEN COALESCE(e.projects_all_time_event, 0) - COALESCE(f.projects_all_time_event,0) < 0 THEN 0
      ELSE COALESCE(e.projects_all_time_event, 0) - COALESCE(f.projects_all_time_event,0)  END AS projects_event_this_month
  , CASE WHEN COALESCE(e.projects_with_repositories_enabled_all_time_event, 0) - COALESCE(f.projects_with_repositories_enabled_all_time_event,0) < 0 THEN 0
      ELSE COALESCE(e.projects_with_repositories_enabled_all_time_event, 0) - COALESCE(f.projects_with_repositories_enabled_all_time_event,0)  END AS projects_with_repositories_enabled_event_this_month
  , CASE WHEN COALESCE(e.protected_branches_all_time_event, 0) - COALESCE(f.protected_branches_all_time_event,0) < 0 THEN 0
      ELSE COALESCE(e.protected_branches_all_time_event, 0) - COALESCE(f.protected_branches_all_time_event,0)  END AS protected_branches_event_this_month
  , CASE WHEN COALESCE(e.ci_pipelines_all_time_user, 0) - COALESCE(f.ci_pipelines_all_time_user,0) < 0 THEN 0
      ELSE COALESCE(e.ci_pipelines_all_time_user, 0) - COALESCE(f.ci_pipelines_all_time_user,0)  END AS ci_pipelines_all_time_user_this_month
  , CASE WHEN COALESCE(e.projects_with_repositories_enabled_all_time_user, 0) - COALESCE(f.projects_with_repositories_enabled_all_time_user,0) < 0 THEN 0
      ELSE COALESCE(e.projects_with_repositories_enabled_all_time_user, 0) - COALESCE(f.projects_with_repositories_enabled_all_time_user,0)  END AS projects_with_repositories_enabled_all_time_user_this_month
  , CASE WHEN COALESCE(e.dependency_scanning_jobs_all_time_user, 0) - COALESCE(f.dependency_scanning_jobs_all_time_user,0) < 0 THEN 0
      ELSE COALESCE(e.dependency_scanning_jobs_all_time_user, 0) - COALESCE(f.dependency_scanning_jobs_all_time_user,0)  END AS dependency_scanning_jobs_all_time_user_this_month
  , CASE WHEN COALESCE(e.auto_devops_pipelines_all_time_event, 0) - COALESCE(f.auto_devops_pipelines_all_time_event,0) < 0 THEN 0
      ELSE COALESCE(e.auto_devops_pipelines_all_time_event, 0) - COALESCE(f.auto_devops_pipelines_all_time_event,0) END AS auto_devops_pipelines_event_this_month

  --Pull in stage usage data so far this month
  , CASE WHEN COALESCE(g.stage_plan_alltime_feature_sum,0) - COALESCE(h.stage_plan_alltime_feature_sum,0)  < 0 THEN 0
      ELSE COALESCE(g.stage_plan_alltime_feature_sum,0) - COALESCE(h.stage_plan_alltime_feature_sum,0)   END AS stage_plan_feature_sum_this_month
  , CASE WHEN COALESCE(g.stage_create_alltime_feature_sum,0) - COALESCE(h.stage_create_alltime_feature_sum,0)  < 0 THEN 0
      ELSE COALESCE(g.stage_create_alltime_feature_sum,0) - COALESCE(h.stage_create_alltime_feature_sum,0)   END AS stage_create_feature_sum_this_month
  , CASE WHEN COALESCE(g.stage_verify_alltime_feature_sum,0) - COALESCE(h.stage_verify_alltime_feature_sum,0)  < 0 THEN 0
      ELSE COALESCE(g.stage_verify_alltime_feature_sum,0) - COALESCE(h.stage_verify_alltime_feature_sum,0)   END AS stage_verify_feature_sum_this_month
  , CASE WHEN COALESCE(g.stage_package_alltime_feature_sum,0) - COALESCE(h.stage_package_alltime_feature_sum,0)  < 0 THEN 0
      ELSE COALESCE(g.stage_package_alltime_feature_sum,0) - COALESCE(h.stage_package_alltime_feature_sum,0)  END AS stage_package_feature_sum_this_month
  , CASE WHEN COALESCE(g.stage_release_alltime_feature_sum,0) - COALESCE(h.stage_release_alltime_feature_sum,0)  < 0 THEN 0
      ELSE COALESCE(g.stage_release_alltime_feature_sum,0) - COALESCE(h.stage_release_alltime_feature_sum,0)   END AS stage_release_feature_sum_this_month
  , CASE WHEN COALESCE(g.stage_configure_alltime_features_sum,0) - COALESCE(h.stage_configure_alltime_features_sum,0)  < 0 THEN 0
      ELSE COALESCE(g.stage_configure_alltime_features_sum,0) - COALESCE(h.stage_configure_alltime_features_sum,0)   END AS stage_configure_features_sum_this_month
  , CASE WHEN COALESCE(g.stage_monitor_alltime_features_sum,0) - COALESCE(h.stage_monitor_alltime_features_sum,0)  < 0 THEN 0
      ELSE COALESCE(g.stage_monitor_alltime_features_sum,0) - COALESCE(h.stage_monitor_alltime_features_sum,0)   END AS stage_monitor_features_sum_this_month
  , CASE WHEN COALESCE(g.stage_manage_alltime_feature_sum,0) - COALESCE(h.stage_manage_alltime_feature_sum,0)  < 0 THEN 0
      ELSE COALESCE(g.stage_manage_alltime_feature_sum,0) - COALESCE(h.stage_manage_alltime_feature_sum,0)   END AS stage_manage_feature_sum_this_month
  , CASE WHEN COALESCE(g.stage_secure_alltime_feature_sum,0) - COALESCE(h.stage_secure_alltime_feature_sum,0)  < 0 THEN 0
      ELSE COALESCE(g.stage_secure_alltime_feature_sum,0) - COALESCE(h.stage_secure_alltime_feature_sum,0)   END AS stage_secure_feature_sum_this_month
  , CASE WHEN COALESCE(g.stage_protect_alltime_feature_sum,0) - COALESCE(h.stage_protect_alltime_feature_sum,0)  < 0 THEN 0
      ELSE COALESCE(g.stage_protect_alltime_feature_sum,0) - COALESCE(h.stage_protect_alltime_feature_sum,0)   END AS stage_protect_feature_sum_this_month
  , CASE WHEN COALESCE(g.stage_ecosystem_alltime_feature_sum,0) - COALESCE(h.stage_ecosystem_alltime_feature_sum,0)  < 0 THEN 0
      ELSE COALESCE(g.stage_ecosystem_alltime_feature_sum,0) - COALESCE(h.stage_ecosystem_alltime_feature_sum,0)   END AS stage_ecosystem_feature_sum_this_month
  , CASE WHEN COALESCE(g.stage_growth_alltime_feature_sum,0) - COALESCE(h.stage_growth_alltime_feature_sum,0)  < 0 THEN 0
      ELSE COALESCE(g.stage_growth_alltime_feature_sum,0) - COALESCE(h.stage_growth_alltime_feature_sum,0)   END AS stage_growth_feature_sum_this_month
  , CASE WHEN COALESCE(g.stage_enablement_alltime_feature_sum,0) - COALESCE(h.stage_enablement_alltime_feature_sum,0)  < 0 THEN 0
      ELSE COALESCE(g.stage_enablement_alltime_feature_sum,0) - COALESCE(h.stage_enablement_alltime_feature_sum,0)  END AS stage_enablement_feature_sum_this_month
  , CASE WHEN COALESCE(g.all_stages_alltime_feature_sum,0) - COALESCE(h.all_stages_alltime_feature_sum,0)  < 0 THEN 0
      ELSE COALESCE(g.all_stages_alltime_feature_sum,0) - COALESCE(h.all_stages_alltime_feature_sum,0)   END AS all_stages_feature_sum_this_month


  -- Have they recently used certain product features
  , CASE WHEN COALESCE(e.action_monthly_active_users_project_repo_28_days_user, f.action_monthly_active_users_project_repo_28_days_user) > 0 THEN 1 ELSE 0 END AS action_mau_project_repo_recent_use_flag
  , CASE WHEN COALESCE(e.merge_requests_28_days_user, f.merge_requests_28_days_user) > 0 THEN 1 ELSE 0 END AS merge_requests_recent_use_flag
  , CASE WHEN COALESCE(e.projects_with_repositories_enabled_28_days_user, f.projects_with_repositories_enabled_28_days_user) > 0 THEN 1 ELSE 0 END AS projects_with_repos_enabled_recent_use_flag
  , CASE WHEN COALESCE(e.ci_pipelines_28_days_user, f.ci_pipelines_28_days_user) > 0 THEN 1 ELSE 0 END AS ci_pipelines_recent_use_flag
  , CASE WHEN COALESCE(e.ci_internal_pipelines_28_days_user, f.ci_internal_pipelines_28_days_user) > 0 THEN 1 ELSE 0 END AS ci_internal_pipelines_recent_use_flag
  , CASE WHEN COALESCE(e.ci_builds_28_days_user, f.ci_builds_28_days_user) > 0 THEN 1 ELSE 0 END AS ci_builds_recent_use_flag
  , CASE WHEN COALESCE(e.ci_pipeline_config_repository_28_days_user, f.ci_pipeline_config_repository_28_days_user) > 0 THEN 1 ELSE 0 END AS ci_pipeline_config_repository_recent_use_flag
  , CASE WHEN COALESCE(e.user_unique_users_all_secure_scanners_28_days_user, f.user_unique_users_all_secure_scanners_28_days_user) > 0 THEN 1 ELSE 0 END AS user_unique_users_all_secure_scanners_recent_use_flag
  , CASE WHEN COALESCE(e.user_sast_jobs_28_days_user, f.user_sast_jobs_28_days_user) > 0 THEN 1 ELSE 0 END AS user_sast_jobs_recent_use_flag
  , CASE WHEN COALESCE(e.user_dast_jobs_28_days_user, f.user_dast_jobs_28_days_user) > 0 THEN 1 ELSE 0 END AS user_dast_jobs_recent_use_flag
  , CASE WHEN COALESCE(e.user_dependency_scanning_jobs_28_days_user, f.user_dependency_scanning_jobs_28_days_user) > 0 THEN 1 ELSE 0 END AS user_dependency_scanning_jobs_recent_use_flag
  , CASE WHEN COALESCE(e.user_license_management_jobs_28_days_user, f.user_license_management_jobs_28_days_user) > 0 THEN 1 ELSE 0 END AS user_license_management_jobs_recent_use_flag
  , CASE WHEN COALESCE(e.user_secret_detection_jobs_28_days_user, f.user_secret_detection_jobs_28_days_user) > 0 THEN 1 ELSE 0 END AS user_secret_detection_jobs_recent_use_flag
  , CASE WHEN COALESCE(e.user_container_scanning_jobs_28_days_user, f.user_container_scanning_jobs_28_days_user) > 0 THEN 1 ELSE 0 END AS user_container_scanning_jobs_recent_use_flag
  , CASE WHEN COALESCE(e.projects_with_packages_28_days_user, f.projects_with_packages_28_days_user) > 0 THEN 1 ELSE 0 END AS projects_with_packages_recent_use_flag
  , CASE WHEN COALESCE(e.deployments_28_days_user, f.deployments_28_days_user) > 0 THEN 1 ELSE 0 END AS deployments_recent_use_flag
  , CASE WHEN COALESCE(e.releases_28_days_user, f.releases_28_days_user) > 0 THEN 1 ELSE 0 END AS releases_recent_use_flag
  , CASE WHEN COALESCE(e.epics_28_days_user, f.epics_28_days_user) > 0 THEN 1 ELSE 0 END AS epics_recent_use_flag
  , CASE WHEN COALESCE(e.issues_28_days_user, f.issues_28_days_user) > 0 THEN 1 ELSE 0 END AS issues_recent_use_flag
  , CASE WHEN COALESCE(e.deployments_28_days_event, f.deployments_28_days_event) > 0 THEN 1 ELSE 0 END AS deployments_28_days_event_recent_use_flag
  , CASE WHEN COALESCE(e.packages_28_days_event, f.packages_28_days_event) > 0 THEN 1 ELSE 0 END AS packages_28_days_event_recent_use_flag
  , CASE WHEN COALESCE(e.projects_enforcing_code_owner_approval_28_days_user, f.projects_enforcing_code_owner_approval_28_days_user) > 0 THEN 1 ELSE 0 END AS projects_enforcing_code_owner_approval_recent_use_flag
  , CASE WHEN COALESCE(e.project_clusters_enabled_28_days_user, f.project_clusters_enabled_28_days_user) > 0 THEN 1 ELSE 0 END AS project_clusters_enabled_recent_use_flag
  , CASE WHEN COALESCE(e.analytics_28_days_user, f.analytics_28_days_user) > 0 THEN 1 ELSE 0 END AS analytics_recent_use_flag
  , CASE WHEN COALESCE(e.issues_edit_28_days_user, f.issues_edit_28_days_user) > 0 THEN 1 ELSE 0 END AS issues_edit_recent_use_flag
  , CASE WHEN COALESCE(e.user_packages_28_days_user, f.user_packages_28_days_user) > 0 THEN 1 ELSE 0 END AS user_packages_recent_use_flag
  , CASE WHEN COALESCE(e.merge_requests_with_required_code_owners_28_days_user, f.merge_requests_with_required_code_owners_28_days_user) > 0 THEN 1 ELSE 0 END AS merge_requests_with_required_code_owners_recent_use_flag
  , CASE WHEN COALESCE(e.code_review_user_approve_mr_28_days_user, f.code_review_user_approve_mr_28_days_user) > 0 THEN 1 ELSE 0 END AS code_review_user_approve_mr_recent_use_flag
  , CASE WHEN COALESCE(e.epics_usage_28_days_user, f.epics_usage_28_days_user) > 0 THEN 1 ELSE 0 END AS epics_usage_recent_use_flag
  , CASE WHEN COALESCE(e.container_scanning_pipeline_usage_28_days_event, f.container_scanning_pipeline_usage_28_days_event) > 0 THEN 1 ELSE 0 END AS container_scanning_pipeline_usage_28_days_event_recent_use_flag
  , CASE WHEN COALESCE(e.dependency_scanning_pipeline_usage_28_days_event, f.dependency_scanning_pipeline_usage_28_days_event) > 0 THEN 1 ELSE 0 END AS dependency_scanning_pipeline_usage_28_days_event_recent_use_flag
  , CASE WHEN COALESCE(e.sast_pipeline_usage_28_days_event, f.sast_pipeline_usage_28_days_event) > 0 THEN 1 ELSE 0 END AS sast_pipeline_usage_28_days_event_recent_use_flag
  , CASE WHEN COALESCE(e.secret_detection_pipeline_usage_28_days_event, f.secret_detection_pipeline_usage_28_days_event) > 0 THEN 1 ELSE 0 END AS secret_detection_pipeline_usage_28_days_event_recent_use_flag
  , CASE WHEN COALESCE(e.dast_pipeline_usage_28_days_event, f.dast_pipeline_usage_28_days_event) > 0 THEN 1 ELSE 0 END AS dast_pipeline_usage_28_days_event_recent_use_flag
  , CASE WHEN COALESCE(e.successful_deployments_28_days_event, f.successful_deployments_28_days_event) > 0 THEN 1 ELSE 0 END AS successful_deployments_28_days_event_recent_use_flag
  , CASE WHEN COALESCE(e.failed_deployments_28_days_event, f.failed_deployments_28_days_event) > 0 THEN 1 ELSE 0 END AS failed_deployments_28_days_event_recent_use_flag
  , CASE WHEN COALESCE(e.successful_deployments_28_days_user, f.successful_deployments_28_days_user) > 0 THEN 1 ELSE 0 END AS successful_deployments_recent_use_flag

  -- Have they recently used any of the stages
  , CASE WHEN COALESCE(g.stage_plan_28days_features, h.stage_plan_28days_features) > 0 THEN 1 ELSE  0 END AS stage_plan_recent_use_flag
  , CASE WHEN COALESCE(g.stage_create_28days_features, h.stage_create_28days_features) > 0 THEN 1 ELSE  0 END AS stage_create_recent_use_flag
  , CASE WHEN COALESCE(g.stage_verify_28days_features, h.stage_verify_28days_features) > 0 THEN 1 ELSE  0 END AS stage_verify_recent_use_flag
  , CASE WHEN COALESCE(g.stage_package_28days_features, h.stage_package_28days_features) > 0 THEN 1 ELSE  0 END AS stage_package_recent_use_flag
  , CASE WHEN COALESCE(g.stage_release_28days_features, h.stage_release_28days_features) > 0 THEN 1 ELSE  0 END AS stage_release_recent_use_flag
  , CASE WHEN COALESCE(g.stage_configure_28days_features, h.stage_configure_28days_features) > 0 THEN 1 ELSE  0 END AS stage_configure_recent_use_flag
  , CASE WHEN COALESCE(g.stage_monitor_28days_features, h.stage_monitor_28days_features) > 0 THEN 1 ELSE  0 END AS stage_monitor_recent_use_flag
  , CASE WHEN COALESCE(g.stage_manage_28days_features, h.stage_manage_28days_features) > 0 THEN 1 ELSE  0 END AS stage_manage_recent_use_flag
  , CASE WHEN COALESCE(g.stage_secure_28days_features, h.stage_secure_28days_features) > 0 THEN 1 ELSE  0 END AS stage_secure_recent_use_flag
  , CASE WHEN COALESCE(g.stage_protect_28days_features, h.stage_protect_28days_features) > 0 THEN 1 ELSE  0 END AS stage_protect_recent_use_flag
  , CASE WHEN COALESCE(g.stage_ecosystem_28days_features, h.stage_ecosystem_28days_features) > 0 THEN 1 ELSE  0 END AS stage_ecosystem_recent_use_flag
  , CASE WHEN COALESCE(g.stage_growth_28days_features, h.stage_growth_28days_features) > 0 THEN 1 ELSE  0 END AS stage_growth_recent_use_flag
  , CASE WHEN COALESCE(g.stage_enablement_28days_features, h.stage_enablement_28days_features) > 0 THEN 1 ELSE  0 END AS stage_enablement_recent_use_flag
  , CASE WHEN COALESCE(g.section_dev_28days_features, h.section_dev_28days_features) > 0 THEN 1 ELSE  0 END AS section_dev_recent_use_flag
  , CASE WHEN COALESCE(g.section_enablement_28days_features, h.section_enablement_28days_features) > 0 THEN 1 ELSE  0 END AS section_enablement_recent_use_flag
  , CASE WHEN COALESCE(g.section_fulfillment_28days_features, h.section_fulfillment_28days_features) > 0 THEN 1 ELSE  0 END AS section_fulfillment_recent_use_flag
  , CASE WHEN COALESCE(g.section_growth_28days_features, h.section_growth_28days_features) > 0 THEN 1 ELSE  0 END AS section_growth_recent_use_flag
  , CASE WHEN COALESCE(g.section_ops_28days_features, h.section_ops_28days_features) > 0 THEN 1 ELSE  0 END AS section_ops_recent_use_flag
  , CASE WHEN COALESCE(g.section_sec_28days_features, h.section_sec_28days_features) > 0 THEN 1 ELSE  0 END AS section_sec_recent_use_flag
  , CASE WHEN COALESCE(g.section_seg_28days_features, h.section_seg_28days_features) > 0 THEN 1 ELSE  0 END AS section_seg_recent_use_flag
  , CASE WHEN COALESCE(g.tier_free_28days_features, h.tier_free_28days_features) > 0 THEN 1 ELSE  0 END AS tier_free_recent_use_flag
  , CASE WHEN COALESCE(g.tier_premium_28days_features, h.tier_premium_28days_features) > 0 THEN 1 ELSE  0 END AS tier_premium_recent_use_flag
  , CASE WHEN COALESCE(g.tier_ultimate_28days_features, h.tier_ultimate_28days_features) > 0 THEN 1 ELSE  0 END AS tier_ultimate_recent_use_flag

-- Usage Order Data
  , COALESCE(i.first_stage_create,0) AS first_stage_create
  , COALESCE(i.first_stage_protect,0) AS first_stage_protect
  , COALESCE(i.first_stage_package,0) AS first_stage_package
  , COALESCE(i.first_stage_monitor,0) AS first_stage_monitor
  , COALESCE(i.first_stage_manage,0) AS first_stage_manage
  , COALESCE(i.first_stage_plan,0) AS first_stage_plan
  , COALESCE(i.first_stage_secure,0) AS first_stage_secure
  , COALESCE(i.first_stage_verify,0) AS first_stage_verify
  , COALESCE(i.first_stage_configure,0) AS first_stage_configure
  , COALESCE(i.first_stage_release,0) AS first_stage_release
    
  , COALESCE(i.second_stage_create,0) AS second_stage_create
  , COALESCE(i.second_stage_protect,0) AS second_stage_protect
  , COALESCE(i.second_stage_package,0) AS second_stage_package
  , COALESCE(i.second_stage_monitor,0) AS second_stage_monitor
  , COALESCE(i.second_stage_manage,0) AS second_stage_manage
  , COALESCE(i.second_stage_plan,0) AS second_stage_plan
  , COALESCE(i.second_stage_secure,0) AS second_stage_secure
  , COALESCE(i.second_stage_verify,0) AS second_stage_verify 
  , COALESCE(i.second_stage_configure,0) AS second_stage_configure
  , COALESCE(i.second_stage_release,0) AS second_stage_release
    
  , COALESCE(i.third_stage_create,0) AS third_stage_create
  , COALESCE(i.third_stage_protect,0) AS third_stage_protect
  , COALESCE(i.third_stage_package,0) AS third_stage_package
  , COALESCE(i.third_stage_monitor,0) AS third_stage_monitor
  , COALESCE(i.third_stage_manage,0) AS third_stage_manage
  , COALESCE(i.third_stage_plan,0) AS third_stage_plan
  , COALESCE(i.third_stage_secure,0) AS third_stage_secure
  , COALESCE(i.third_stage_verify,0) AS third_stage_verify
  , COALESCE(i.third_stage_configure,0) AS third_stage_configure
  , COALESCE(i.third_stage_release,0) AS third_stage_release

  , CONCAT( CASE WHEN i.first_stage_create = 1 THEN '_create' ELSE '' END 
          , CASE WHEN i.first_stage_protect = 1 THEN '_protect' ELSE '' END
          , CASE WHEN i.first_stage_package = 1 THEN '_package' ELSE '' END
          , CASE WHEN i.first_stage_monitor = 1 THEN '_monitor' ELSE '' END
          , CASE WHEN i.first_stage_manage = 1 THEN '_manage' ELSE '' END
          , CASE WHEN i.first_stage_plan = 1 THEN '_plan' ELSE '' END
          , CASE WHEN i.first_stage_secure = 1 THEN '_secure' ELSE '' END
          , CASE WHEN i.first_stage_verify = 1 THEN '_verify' ELSE '' END
          , CASE WHEN i.first_stage_configure = 1 THEN '_configure' ELSE '' END
          , CASE WHEN i.first_stage_release = 1 THEN '_release' ELSE '' END
          )
          AS first_stage_list

  , CONCAT( CASE WHEN i.second_stage_create = 1 THEN '_create' ELSE '' END 
          , CASE WHEN i.second_stage_protect = 1 THEN '_protect' ELSE '' END
          , CASE WHEN i.second_stage_package = 1 THEN '_package' ELSE '' END
          , CASE WHEN i.second_stage_monitor = 1 THEN '_monitor' ELSE '' END
          , CASE WHEN i.second_stage_manage = 1 THEN '_manage' ELSE '' END
          , CASE WHEN i.second_stage_plan = 1 THEN '_plan' ELSE '' END
          , CASE WHEN i.second_stage_secure = 1 THEN '_secure' ELSE '' END
          , CASE WHEN i.second_stage_verify = 1 THEN '_verify' ELSE '' END
          , CASE WHEN i.second_stage_configure = 1 THEN '_configure' ELSE '' END
          , CASE WHEN i.second_stage_release = 1 THEN '_release' ELSE '' END
          )
          AS second_stage_list

  , CONCAT( CASE WHEN i.third_stage_create = 1 THEN '_create' ELSE '' END 
        , CASE WHEN i.third_stage_protect = 1 THEN '_protect' ELSE '' END
        , CASE WHEN i.third_stage_package = 1 THEN '_package' ELSE '' END
        , CASE WHEN i.third_stage_monitor = 1 THEN '_monitor' ELSE '' END
        , CASE WHEN i.third_stage_manage = 1 THEN '_manage' ELSE '' END
        , CASE WHEN i.third_stage_plan = 1 THEN '_plan' ELSE '' END
        , CASE WHEN i.third_stage_secure = 1 THEN '_secure' ELSE '' END
        , CASE WHEN i.third_stage_verify = 1 THEN '_verify' ELSE '' END
        , CASE WHEN i.third_stage_configure = 1 THEN '_configure' ELSE '' END
        , CASE WHEN i.third_stage_release = 1 THEN '_release' ELSE '' END
        )
        AS third_stage_list

FROM trial_namespaces a
LEFT JOIN paid_namespaces b
  ON a.namespace_id = b.namespace_id 
LEFT JOIN creator_data c
  ON a.creator_id = c.creator_id
LEFT JOIN user_data c2
  ON a.namespace_id = c2.namespace_id
LEFT JOIN snowplow_activity d
  ON a.namespace_id = d.namespace_id
LEFT JOIN product_usage_curr_month e
  ON a.namespace_id = e.dim_namespace_id
LEFT JOIN product_usage_prev_month f
  ON a.namespace_id = f.dim_namespace_id
LEFT JOIN stage_usage_curr_month g
  ON a.namespace_id = g.dim_namespace_id
LEFT JOIN stage_usage_prev_month h
  ON a.namespace_id = h.dim_namespace_id
LEFT JOIN usage_order i
  ON a.namespace_id = i.namespace_id
WHERE (e.dim_namespace_id IS NOT NULL OR f.dim_namespace_id IS NOT NULL OR g.dim_namespace_id IS NOT NULL OR h.dim_namespace_id IS NOT NULL ) -- must have product usage data from some point in the last two months
  

   
    
    