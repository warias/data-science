- Both training and scoring use the [pte_base_query.sql macro](https://gitlab.com/gitlab-data/analytics/-/blob/master/transform/snowflake-dbt/macros/workspaces/pte_base_query.sql) to populate data. Query is parameterized to poulate based on the variables contained in the training and scoring notebooks.
- [PtE Project](https://gitlab.com/gitlab-data/propensity-to-buy)

