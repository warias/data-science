def populate_insights_table(model, df, model_name, field_labels):

    import numpy as np
    import pandas as pd
    import shap

    # Create object that can calculate shap values
    explainer = shap.TreeExplainer(model)
    df = df.rename(columns=field_labels)

    # Calculate shap_values for all dataset rather than a single row, to have more data for plot.
    shap_values = explainer.shap_values(df)
    df_with_insights = pd.DataFrame()
    actionable_columns = list(field_labels.values())
    for i in np.arange(0, len(df), step=1):

        # we have numeric index to take correct value from shap
        insights = pd.DataFrame(shap_values[1][i], df.columns)
        insights["model"] = model_name
        insights["account_id"] = df.index[i]
        insights.reset_index(level=0, inplace=True)
        insights.columns = ["feature", "shap_values", "model", "account_id"]

        # keep only actionable columns
        insights = insights.loc[insights["feature"].isin(actionable_columns)]

        # creates relative impact on score based on average score for all accounts from this model
        insights["shap_values"] = insights["shap_values"] / explainer.expected_value[1]

        insights["relative_shap_score"] = (
            insights["shap_values"] / explainer.expected_value[1]
        )

        account_values = df[df.index == df.index[i]]
        feature_value_df = account_values[insights["feature"]].stack().reset_index()
        feature_value_df = pd.DataFrame(feature_value_df)
        feature_value_df.columns = ["account_id", "feature", "feature_value"]

        # keep only actionable features

        feature_value_df = feature_value_df.loc[
            feature_value_df["feature"].isin(actionable_columns)
        ]

        feature_value_df["feature_value"] = np.where(
            feature_value_df["feature"].str.match("%"),
            feature_value_df["feature_value"] * 100,
            feature_value_df["feature_value"],
        )
        feature_value_df["feature_value"] = np.where(
            feature_value_df["feature"].str.match(":"),
            np.floor(feature_value_df["feature_value"]),
            feature_value_df["feature_value"],
        )
        feature_value_df["feature_value"] = np.where(
            feature_value_df["feature"].str.match("users"),
            np.floor(feature_value_df["feature_value"]),
            feature_value_df["feature_value"],
        )
        feature_value_df["feature_value"] = feature_value_df["feature_value"].round(
            decimals=2
        )

        feature_value_df["feature_value"] = feature_value_df["feature_value"].astype(
            str
        )
        feature_value_df["feature_value"] = np.where(
            (feature_value_df["feature"].str.match("flag"))
            & (feature_value_df["feature_value"] == "1.0"),
            "Presence of",
            np.where(
                (feature_value_df["feature"].str.match("flag"))
                & (feature_value_df["feature_value"] == "0.0"),
                "Lack of",
                feature_value_df["feature_value"],
            ),
        )

        shap_quarters = pd.qcut(
            abs(insights["relative_shap_score"]),
            4,
            retbins=True,
            labels=False,
            duplicates="drop",
        )
        shap_quarters = pd.DataFrame(shap_quarters)

        shap_quarters = shap_quarters.T

        shap_quarters["shap_impact_relative"] = np.where(
            shap_quarters["relative_shap_score"] == 0,
            "a little",
            np.where(
                shap_quarters["relative_shap_score"] == 1,
                "somewhat",
                np.where(
                    shap_quarters["relative_shap_score"] == 2, "", "significantly"
                ),
            ),
        )
        shap_quarters.drop(columns=["Unnamed 0", "relative_shap_score"], inplace=True)
        insights = insights.join(shap_quarters, lsuffix="_left", rsuffix="_right")
        insights = insights.loc[insights["feature"].isin(actionable_columns)]

        feature_value_df["feature_value_numeric"] = pd.to_numeric(
            feature_value_df["feature_value"]
        )
        values_quarters = pd.qcut(
            abs(feature_value_df["feature_value_numeric"]),
            4,
            retbins=True,
            labels=False,
            duplicates="drop",
        )
        values_quarters = pd.DataFrame(values_quarters)

        values_quarters = values_quarters.T

        values_quarters["values_impact_relative"] = np.where(
            values_quarters["feature_value_numeric"] == 0,
            "Low",
            np.where(
                values_quarters["feature_value_numeric"] == 1,
                "Low",
                np.where(
                    values_quarters["feature_value_numeric"] == 2, "Moderate", "High"
                ),
            ),
        )

        values_quarters.drop(
            columns=["Unnamed 0", "feature_value_numeric"], inplace=True
        )

        insights = insights.join(values_quarters, lsuffix="_left", rsuffix="_right")

        insights["shap_values"] = (insights["shap_values"] * 100).round(decimals=2)

        # keep only those shap with value higher than 0 as these increase churn
        insights["description"] = np.where(
            insights["shap_values"] > 0,
            "increases likelihood to churn/contract",
            np.where(
                insights["shap_values"] == 0,
                "no impact",
                "decreases likelihood to churn/contract",
            ),
        )

        insights = insights[insights["shap_values"] > 0]
        # remove negative zeroes
        insights["shap_values"] = np.where(
            insights["shap_values"] == 0,
            abs(insights["shap_values"]),
            insights["shap_values"],
        )
        insights["shap_values_relative_desc"] = np.where(
            insights["shap_values"] == 0,
            abs(insights["shap_values"]),
            insights["shap_values"],
        )

        # drop numeric values in here

        insights[insights["account_id"] == df.index[i]]
        insights["rank"] = (
            insights["shap_values"]
            .abs()
            .groupby(insights["account_id"])
            .rank(ascending=False)
        )

        # rank allows to assign how many features will be listed, we keep only top3
        insights["to_drop"] = np.where(insights["rank"] >= 4, 1, 0)
        insights = insights[insights["to_drop"] == 0]

        feature_value_df = feature_value_df[
            feature_value_df["feature"].isin(insights["feature"])
        ]

        insights = insights.merge(
            feature_value_df, on=["account_id", "feature"], how="left"
        )

        remaining_insights = insights.copy()
        remaining_insights.values_impact_relative = (
            remaining_insights.values_impact_relative.fillna("Low")
        )
        remaining_insights["values_impact_relative"] = np.where(
            (remaining_insights["feature"].str.match("(compared to previous month)"))
            & (pd.to_numeric(remaining_insights["feature_value"]) < 0),
            "Decreased",
            remaining_insights["values_impact_relative"],
        )

        remaining_insights = remaining_insights.drop(
            columns=["feature_value", "feature_value_numeric"]
        )
        remaining_insights["insights"] = (
            remaining_insights["values_impact_relative"]
            + " "
            + remaining_insights["feature"]
            + ", "
            + "which "
            + insights["shap_impact_relative"]
            + " "
            + remaining_insights["description"]
        )
        remaining_insights = remaining_insights.drop(
            columns=["rank", "to_drop", "values_impact_relative"]
        )

        insights_grouped = (
            remaining_insights.groupby("account_id")["insights"]
            .apply(". ".join)
            .reset_index()
        )
        final = remaining_insights.merge(insights_grouped, on="account_id", how="left")
        final = final.drop(
            columns=[
                "feature",
                "shap_values",
                "description",
                "insights_x",
                "relative_shap_score",
                "shap_impact_relative",
                "shap_values_relative_desc",
            ]
        )
        final.rename(columns={"insights_y": "insights"}, inplace=True)
        final = final.drop_duplicates()
        df_with_insights = pd.concat([df_with_insights, final])

    return df_with_insights